package com.aoplogback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AopLogBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(AopLogBackApplication.class, args);
	}

}
