package com.aoplogback.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aoplogback.annotation.LogCostTimeAnnotation;
import com.aoplogback.service.DummyService;

@RestController
@RequestMapping("/")
public class DummyController {
	
	@Autowired
	DummyService dummyService;
	
	
	@LogCostTimeAnnotation
    @GetMapping("/index")
    public String index() throws InterruptedException {
        return dummyService.exec1();
    }

}
