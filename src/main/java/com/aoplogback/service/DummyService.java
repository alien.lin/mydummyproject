package com.aoplogback.service;

import org.springframework.stereotype.Service;

import com.aoplogback.annotation.LogCostTimeAnnotation;

@Service
public class DummyService {
	
	@LogCostTimeAnnotation
	public String exec1() {
		return "exec1() perform.......";
	}

}
