package com.aoplogback.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AopService {
	
	/*
	 * Logger===>
	 * 
	 */
	final static Logger LOGGER = LoggerFactory.getLogger(AopService.class);
	
	
	/*
	 * Attributes===>
	 * 
	 */
    @Value("${log.costtime.aop.enable:true}")
    private boolean islogCostTimeAopEnable; //AOP開關
    
    
    
    /*
     * 兩種方法：
     * 1. PointCut目標, 
     * com.aoplogback.controller這package底下所有public method都會作用, 其餘通通不會===>
     */
//    @Pointcut("execution(public * com.aoplogback.controller.*.*(..))")
	/*
	 * 2. PointCut目標, 
	 * 只有被 @LogCostTimeAnnotation標註的method有作用===>
	 */
    @Pointcut("@annotation(com.aoplogback.annotation.LogCostTimeAnnotation)")
    public void costTimePointCut(){
    	
    }

    
    /*
     * @Around有環繞效果, 可以在目標method前和後做額外處理
     * 
     */
    @Around("costTimePointCut()")
	public Object around(ProceedingJoinPoint targetPoint) throws Throwable {
        //開關為false則直接執行, 不跑log
        if(!islogCostTimeAopEnable){
           return targetPoint.proceed();
        }
        long startTime = System.currentTimeMillis();
        //執行目標method
        Object result = targetPoint.proceed();
        //計算耗時
        long costTime = System.currentTimeMillis() - startTime;
        showLog(targetPoint, costTime);
        return result;
	}
    
    
    
    /*
     * Private Operation===>
     * 
     */
    private void showLog(ProceedingJoinPoint targetPoint, long costTime) {
        MethodSignature signature = (MethodSignature) targetPoint.getSignature();
        String className = targetPoint.getTarget().getClass().getName();
        LOGGER.info("{}.{} cost :{}ms", className, signature.getName(), costTime);
    }
	
}
